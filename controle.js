$( document ).ready(function() {
    getContatos();

    $('#addContactButton').on('click', function(event) {
        event.preventDefault();
        $('#cadastro').fadeIn("fast");
    });

    $('#addContact').on('click', function() {
        adicionarContato();
    });

    $(document).on('click','.name',function(){
        if(!$(this).hasClass("ativo")){
            $(this).addClass("ativo");
            var nome = $(this).text();
            $(this).text("");
            $(this).append("<input type='text' class='editaNome' value=''>");
            var inPutEditaNome = $(this).closest('div').find(".editaNome");
            inPutEditaNome.focus().val(nome);
        }
    });

    $(document).on('click','.tel',function(){
        if(!$(this).hasClass("ativo")){
            $(this).addClass("ativo");
            var telefone = $(this).text();
            $(this).text("");
            $(this).append("<input type='text' class='editaTelefone' value=''>");
            var inPutEditaTel = $(this).closest('div').find(".editaTelefone");
            inPutEditaTel.inputmask({"mask": "(99) 99999-9999"});
            inPutEditaTel.focus().val(telefone);
        }
    });

    $(document).on('keypress','.editaNome',function(){
        if (event.which === 13) {
            var nome = $(this).val();
            spamName = $(this).closest(".name");
            var id = spamName.attr('id');
            editaNomeContato(id, nome);
            spamName.removeClass("ativo");
            spamName.text(nome);
        }
    });

    $(document).on('keypress','.editaTelefone',function(){
        if (event.which === 13) {
            var telefoneFormatado = $(this).val();
            var telefoneLimpo = limpaTelefone(telefoneFormatado);
            if(telefoneLimpo.length == 11) {
                spamName = $(this).closest('div').find(".name");
                var id = spamName.attr('id');
                editaTelefoneContato(id, telefoneLimpo);
                spamTel = $(this).closest(".tel");
                spamTel.removeClass("ativo");
                spamTel.text(telefoneFormatado);
            }else{
                alert("O telefone deve conter todos os dígitos");
            }
        }
    });

    $(document).on('click','.removeContato',function(){
        var idContato = $(this).closest('div').find(".name").attr('id');
        $.ajax({
            url: "gerenciador.php",
            method: "POST",
            data: {
                "id": idContato,
                "funcao": "removeContato"
            },
            success: function (data) {
                //alert('success');
            }
        }).then(function () {
            getContatos();
        }, function (error) {
            alert("ERRO REMOÇÃO" + JSON.stringify(error));
        })
    });

    $("#telefone, .editaTelefone").inputmask({"mask": "(99) 99999-9999"});

    $('#telefone').on('keypress', function(event) {
        if (event.which === 13) {
            adicionarContato();
        }
    });

    function limpaTelefone(telefone){
        return telefone.replace(/[()_ -]/g,'');
    }

    function formataTelefone(telefone){
        return telefone.replace(/(\d{2})(\d{5})(\d{4})/, "($1) $2-$3");
    }

    function getContatos(){
        $.ajax({
            url: "gerenciador.php",
            method: "POST",
            data: {funcao: "getContatos" },
            dataType: "json"
        }).then(function(response) {
            // alert(JSON.stringify(response));
            atualizaTela(response)
        }, function(error){
            alert("ERRO GET"+JSON.stringify(error));
        });
    }

    function adicionarContato(){
        var inputNome = $('#contact-list-search');
        var inputTelefone = $('#telefone');
        var nome = inputNome.val();
        var telefone = limpaTelefone(inputTelefone.val());
        if(nome != "") {
            if (telefone.length == 11) {
                $.ajax({
                    url: "gerenciador.php",
                    method: "POST",
                    data: {
                        "nome": nome,
                        "telefone": telefone,
                        "funcao": "criaContato"
                    },
                    success: function (data) {
                        //alert('success');
                    }
                }).then(function () {
                    getContatos();
                }, function (error) {
                    alert("ERRO ADD" + JSON.stringify(error));
                });
                $('#cadastro').fadeOut("fast");
                inputNome.val('');
                inputTelefone.val("");
            } else {
                alert("O telefone deve conter todos os dígitos");
            }
        }else{
            alert("Indique um nome para o contato");
        }
    }

    function editaNomeContato(id, nome){
        $.ajax({
            url: "gerenciador.php",
            method: "POST",
            data: {
                "nome": nome,
                "id": id,
                "funcao": "editaNomeContato"
            },
            success: function () {
                //alert('success');
            }
        }).then(function (response) {
        }, function (error) {
            alert("ERRO EditaNome" + JSON.stringify(error));
        })
    }

    function editaTelefoneContato(id, telefone){
        $.ajax({
            url: "gerenciador.php",
            method: "POST",
            data: {
                "telefone": telefone,
                "id": id,
                "funcao": "editaTelefoneContato"
            },
            success: function (data) {
                //alert('success');
            }
        }).then(function (response) {
        }, function (error) {
            alert("ERRO EditaTelefone" + JSON.stringify(error));
        })
    }

    function atualizaTela(dados){
        $('#contact-list').empty();
        $.each(dados, function (index, value) {
            var telefoneFormatado = formataTelefone(value.telefone);
            var $linha = $("<li class='list-group-item'><div class='col-xs-12'><span class='name' title='Clique para editar e aperte ENTER' id='"+value.id+"'>"+value.nome+"</span><br/><span class='glyphicon glyphicon-earphone text-muted c-info' data-toggle='tooltip'></span><span class=''> <span title='Clique para editar e aperte ENTER' class='text-muted tel'>"+telefoneFormatado+"        </span></span><a data-placement='top' title='Remover Contato'><i class='glyphicon glyphicon-trash removeContato'></i></a></div><div class='clearfix'></div></li>");
            $('#contact-list').append($linha);
        })
    }

    $('#contact-list-search').keyup(function(){
        var current_query = $('#contact-list-search').val();
        if (current_query !== "") {
            $(".list-group-item").hide();
            $(".list-group-item").each(function(){
                var current_keyword = $(this).text();
                if (current_keyword.toLowerCase().indexOf(current_query.toLowerCase()) >=0) {
                    $(this).show();
                }
            })
        } else {
            $(".list-group-item").show();
        }
        var contatos = $('#contact-list').children(":visible").length;
        if(contatos == 0){
            $('#cadastro').fadeIn("fast");
            $('#searchButton').prop('disabled', true);
        }else{
            $('#cadastro').fadeOut("fast");
            $('#searchButton').prop('disabled', false);
        }
    });
});