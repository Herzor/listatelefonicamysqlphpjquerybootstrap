<?php
include("ListaTelefonica.php");

$lista = new ListaTelefonica();
if ($_POST){
    $operacao = $_POST["funcao"];
} else {
    echo("Ocorreu um erro na requisição");
    exit;
}

switch ($operacao){
    case "criaContato":
        $lista->criarContato($_POST["nome"],$_POST["telefone"]);
        break;
    case "getContatos":
        $resposta = $lista->getContatos();
        $arrayResposta = array();
        if(mysqli_num_rows($resposta) != 0) {
            while ($linha = mysqli_fetch_assoc($resposta)) {
                $arrayResposta[] = $linha;
            }
        }
        echo $json_info = json_encode($arrayResposta);
        break;
    case "editaNomeContato":
        $lista->atualizarNomeContato($_POST["id"], $_POST["nome"]);
        break;
    case "editaTelefoneContato":
        $lista->atualizarTelefoneContato($_POST["id"], $_POST["telefone"]);
        break;
    case "removeContato":
        $lista->removerContato($_POST["id"]);
        break;
}