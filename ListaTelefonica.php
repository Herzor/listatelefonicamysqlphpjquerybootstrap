<?php
class ListaTelefonica{

    public function conectarBD(){
        return new mysqli('127.0.0.1', 'root', '', 'db');
    }

    public function criarContato($nome, $telefone){
        $bd = $this->conectarBD();
        if(strlen($telefone)== 11 && is_numeric($telefone)){
            $sql = "INSERT INTO contato (nome, telefone) VALUES ('$nome','$telefone')";
            $retorno = $bd->query($sql);
            if ($retorno) {
    //        return $retorno;
            } else {
                echo "Erro: " . $sql . "<br>" . $bd->error;
            }
        }else{
            echo("Erro: Telefone Inválido");
        }

    }
    public function getContatos(){
        $bd = $this->conectarBD();
        $sql = "SELECT * FROM contato ORDER BY nome";
        return $bd->query($sql);
    }
    public function removerContato($id){
        $bd = $this->conectarBD();
        $sql = "DELETE FROM contato WHERE contato.id = '$id'";
        if ($bd->query($sql)) {
            //echo "contato removido";
        } else {
            echo "Erro: " . $sql . "<br>" . $bd->error;
        }
    }

    public function atualizarNomeContato($id, $nome){
        $bd = $this->conectarBD();
        $sql = "UPDATE contato set nome = '$nome' WHERE contato.id = '$id'";
        if ($bd->query($sql)) {
            //echo "nome contato atualizado";
        } else {
            echo "Erro: " . $sql . "<br>" . $bd->error;
        }
    }

    public function atualizarTelefoneContato($id, $telefone){
        $bd = $this->conectarBD();
        if(strlen($telefone)== 11 && is_numeric($telefone)){
            $sql = "UPDATE contato set telefone = '$telefone' WHERE contato.id = '$id'";
            if ($bd->query($sql)) {
                //echo "telefone contato atualizado";
            } else {
                echo "Erro: " . $sql . "<br>" . $bd->error;
            }
        }else{
            echo("Erro: Telefone Inválido");
        }
    }
}
